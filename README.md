# UK distance finder

Uk distance finder is RESTful api using Spring-boot and in-memory H2 db to return the geographic (straight line) distance between two postal codes in the UK.

# Feature available in version 0.0.1
  - get postcode by id
  - put postcode by id
  - find distance between two postcode
### Installation

UK distance finder API requires [Java SE8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Install the artifact and and start the embedded application server.

```sh
$ cd uk-distance-finder/
$ mvn clean package
$ cd target/
$ java -jar uk-distance-finder-0.0.1.jar
```
The application use [lombok plugin](https://projectlombok.org/) , that means that if you checkout the code into any IDE probably the application cannot compile until you install the lombok plugin.
Otherhand you are still able to run the maven goal in a terminal without any issue.
Lombok is available for all IDE here: [lombok plugin IDEs](https://projectlombok.org/setup/overview)


### Endpoint

- GET localhost:8080/api/postcodes?from={postcodeId}&to={postcodeId}
- GET localhost:8080/api/postcodes/{postcodeId}}
- PUT localhost:8080/api/postcodes/{postcodeId}

please for PUT calls set the header **Content-Type**: `application/json `

All the endpoints are secured with `HTTP Basic-auth` the `user` is **user1** and the password is **secret1**

### Data for testing

When the application start _Spring_ load into the in-memory H2 database a subset of 100 UK postcode taken from big original archive from _http://www.freemaptools.com/download-uk-postcode-lat-lng.htm;_

You can have a look to these data in: `uk-distance-finder/src/main/resources/data.sql`

Same subset of data is used for the integration-test.

You can also access the H2 web console here: `localhost:8080/console` and use: `jdbc:h2:mem:uk_distance_finder_db`

```sh
$ cd uk-distance-finder/src/main/resources
```
- Inside the directory above you will find `uk-distance-finder.postman_collection.json` to import in your [POSTMAN](https://www.getpostman.com/) and have all the endpoints, body configured. 


#### Open points and features released in future version

- nice to have integration test for the controller layer using mockMVC
- javadoc everywhere
- real DB integration maybe with PostgresSQL with postgis that handles geospatial data
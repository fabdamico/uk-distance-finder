package com.worth.systems.assessment.ukdistancefinder.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
@Builder
public class PostcodeView {
    @NotNull(message = "postcode cannot be null!")
    private String postcode;

    @NotNull(message = "Longitude cannot be null!")
    private Double longitude;

    @NotNull(message = "Latitude cannot be null!")
    private Double latitude;
}

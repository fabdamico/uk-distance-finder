package com.worth.systems.assessment.ukdistancefinder.mapper;

import com.worth.systems.assessment.ukdistancefinder.dto.DistanceView;
import com.worth.systems.assessment.ukdistancefinder.dto.PostcodeView;
import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;

public class PostcodeMapper {

    public static DistanceView mapPostcodeIntoGeoResource(Postcode originPostcode, Postcode toPostcode, double distance) {
        return DistanceView.builder()
                .unit("km")
                .distance(distance)
                .originLatitude(originPostcode.getLatitude())
                .originLongitude(originPostcode.getLongitude())
                .originPostalCode(originPostcode.getPostCode())
                .toLongitude(toPostcode.getLongitude())
                .toLatitude(toPostcode.getLatitude())
                .toPostalCode(toPostcode.getPostCode()).build();
    }

    public static PostcodeView mapPostcodeIntoPostcodeView(Postcode postcode) {
        return PostcodeView.builder()
                .latitude(postcode.getLatitude())
                .longitude(postcode.getLongitude())
                .postcode(postcode.getPostCode()).build();
    }
}

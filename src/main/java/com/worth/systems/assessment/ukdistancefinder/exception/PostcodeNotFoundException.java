package com.worth.systems.assessment.ukdistancefinder.exception;

public class PostcodeNotFoundException extends Exception {

    public PostcodeNotFoundException(String message) {
        super(message);
    }
}

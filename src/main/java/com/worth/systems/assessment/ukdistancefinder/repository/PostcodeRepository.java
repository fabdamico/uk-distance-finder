package com.worth.systems.assessment.ukdistancefinder.repository;

import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostcodeRepository extends JpaRepository<Postcode, Long> {

    Optional<Postcode> findBypostCode(String postCode);
}

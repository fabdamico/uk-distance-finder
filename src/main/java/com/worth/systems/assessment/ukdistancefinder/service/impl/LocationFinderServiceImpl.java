package com.worth.systems.assessment.ukdistancefinder.service.impl;

import com.worth.systems.assessment.ukdistancefinder.dto.DistanceView;
import com.worth.systems.assessment.ukdistancefinder.dto.PostcodeView;
import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;
import com.worth.systems.assessment.ukdistancefinder.exception.PostcodeNotFoundException;
import com.worth.systems.assessment.ukdistancefinder.mapper.PostcodeMapper;
import com.worth.systems.assessment.ukdistancefinder.repository.PostcodeRepository;
import com.worth.systems.assessment.ukdistancefinder.service.LocationFinderService;
import com.worth.systems.assessment.ukdistancefinder.util.HaversineCalculator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class LocationFinderServiceImpl implements LocationFinderService {

    private PostcodeRepository postcodeRepository;
    private HaversineCalculator haversineCalculator = new HaversineCalculator();

    @Autowired
    public LocationFinderServiceImpl(PostcodeRepository postcodeRepository) {
        this.postcodeRepository = postcodeRepository;
    }

    @Override
    public DistanceView calculateDistance(String postcodeOne, String postcodeTwo) throws PostcodeNotFoundException {

        Optional<Postcode> originPostcode = postcodeRepository.findBypostCode(postcodeOne);
        Optional<Postcode> toPostcode = postcodeRepository.findBypostCode(postcodeTwo);

        if (!originPostcode.isPresent()) {
            log.info("Postcode {} not found in the DB", postcodeOne);
            throw new PostcodeNotFoundException(postcodeOne + " not found in the DB");
        }

        if (!toPostcode.isPresent()) {
            log.info("Postcode {} not found in the DB", postcodeTwo);
            throw new PostcodeNotFoundException(postcodeTwo + " not found in the DB");
        }

        double distance = haversineCalculator.calculateDistance(originPostcode.get().getLatitude(),
                originPostcode.get().getLongitude(),
                toPostcode.get().getLatitude(),
                toPostcode.get().getLongitude());

        return PostcodeMapper.mapPostcodeIntoGeoResource(originPostcode.get(), toPostcode.get(), distance);
    }

    @Override
    public PostcodeView getPostcode(String postcode) throws PostcodeNotFoundException {
        Optional<Postcode> postcodeOptional = postcodeRepository.findBypostCode(postcode);

        if (!postcodeOptional.isPresent()) {
            log.info("Postcode {} not found in the DB", postcode);
            throw new PostcodeNotFoundException(postcode + " not found in the DB");
        }

        return PostcodeMapper.mapPostcodeIntoPostcodeView(postcodeOptional.get());
    }

    @Override
    @Transactional
    public void putPostcode(PostcodeView postcodeView) throws PostcodeNotFoundException {
        Optional<Postcode> postcodeOptional = postcodeRepository.findBypostCode(postcodeView.getPostcode());

        if (!postcodeOptional.isPresent()) {
            log.info("Postcode {} not found in the DB", postcodeView.getPostcode());
            throw new PostcodeNotFoundException(postcodeView.getPostcode() + " not found in the DB");
        }

        postcodeOptional.get().setLatitude(postcodeView.getLatitude());
        postcodeOptional.get().setLongitude(postcodeView.getLongitude());

        postcodeRepository.save(postcodeOptional.get());

    }


}

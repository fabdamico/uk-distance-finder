package com.worth.systems.assessment.ukdistancefinder.service;

import com.worth.systems.assessment.ukdistancefinder.dto.DistanceView;
import com.worth.systems.assessment.ukdistancefinder.dto.PostcodeView;
import com.worth.systems.assessment.ukdistancefinder.exception.PostcodeNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface LocationFinderService {

    DistanceView calculateDistance(String postcode1, String postcode2) throws PostcodeNotFoundException;
    PostcodeView getPostcode(String postcode) throws PostcodeNotFoundException;
    void putPostcode(PostcodeView postcodeView) throws PostcodeNotFoundException;
}

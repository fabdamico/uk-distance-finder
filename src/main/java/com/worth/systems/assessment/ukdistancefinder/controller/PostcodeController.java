package com.worth.systems.assessment.ukdistancefinder.controller;

import com.worth.systems.assessment.ukdistancefinder.dto.DistanceView;
import com.worth.systems.assessment.ukdistancefinder.dto.PostcodeView;
import com.worth.systems.assessment.ukdistancefinder.exception.PostcodeNotFoundException;
import com.worth.systems.assessment.ukdistancefinder.service.LocationFinderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/postcodes/")
public class PostcodeController {

    private LocationFinderService locationFinderService;

    @Autowired
    public PostcodeController(LocationFinderService locationFinderService) {
        this.locationFinderService = locationFinderService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public DistanceView getDistanceBetweenPostcodes(@RequestParam(name = "from") String firstPostcode,
                                                    @RequestParam(name = "to") String secondPostcode) throws PostcodeNotFoundException {
        return locationFinderService.calculateDistance(firstPostcode, secondPostcode);
    }

    @GetMapping(value = "/{postcodeId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PostcodeView getPostcode(@PathVariable String postcodeId) throws PostcodeNotFoundException {
        return locationFinderService.getPostcode(postcodeId);
    }

    @PutMapping(value = "/{postcodeId}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePostcode(@Valid @RequestBody PostcodeView postcodeView) throws PostcodeNotFoundException {
        locationFinderService.putPostcode(postcodeView);
    }
}

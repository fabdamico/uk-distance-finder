package com.worth.systems.assessment.ukdistancefinder.dto;

import lombok.*;

@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DistanceView {

    private String originPostalCode;
    private String toPostalCode;
    private Double originLatitude;
    private Double originLongitude;
    private Double toLatitude;
    private Double toLongitude;
    private String unit;
    private Double distance;
}

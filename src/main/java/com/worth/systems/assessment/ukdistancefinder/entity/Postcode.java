package com.worth.systems.assessment.ukdistancefinder.entity;

import lombok.*;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "postcodes")
@Entity
public class Postcode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "postcode")
    private String postCode;

    @Column
    private Double latitude;

    @Column
    private Double longitude;
}

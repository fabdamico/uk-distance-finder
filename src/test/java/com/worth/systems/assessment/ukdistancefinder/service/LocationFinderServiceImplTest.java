package com.worth.systems.assessment.ukdistancefinder.service;

import com.worth.systems.assessment.ukdistancefinder.dto.DistanceView;
import com.worth.systems.assessment.ukdistancefinder.dto.PostcodeView;
import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;
import com.worth.systems.assessment.ukdistancefinder.exception.PostcodeNotFoundException;
import com.worth.systems.assessment.ukdistancefinder.repository.PostcodeRepository;
import com.worth.systems.assessment.ukdistancefinder.service.impl.LocationFinderServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocationFinderServiceImplTest {

    @Mock
    private PostcodeRepository postcodeRepository;

    @InjectMocks
    private LocationFinderService locationFinderService = new LocationFinderServiceImpl(postcodeRepository);

    @Test
    public void calculateDistanceShouldReturnDistance() throws PostcodeNotFoundException {
        Postcode postcodeMock = Postcode.builder().latitude(57.14416516).longitude(-2.114847768).postCode("AB10 1XG").build();
        Postcode postcodeMockTwo = Postcode.builder().latitude(57.13787976).longitude(-2.121486688).postCode("AB10 6RN").build();

        when(postcodeRepository.findBypostCode(anyString())).thenReturn(Optional.ofNullable(postcodeMock),Optional.ofNullable(postcodeMockTwo));

        DistanceView distance = locationFinderService.calculateDistance("AB10 1XG", "AB10 6RN");
        verify(postcodeRepository, times(2)).findBypostCode(anyString());
        assertThat(distance, is(notNullValue()));

    }

    @Test(expected = PostcodeNotFoundException.class)
    public void calculateDistanceShouldThrownPostcodeNotFoundException() throws PostcodeNotFoundException {
        when(postcodeRepository.findBypostCode(anyString())).thenReturn(Optional.empty());
        locationFinderService.calculateDistance("AB10 1XG", "AB10 6RN");
        verify(postcodeRepository, times(1)).findBypostCode(anyString());
    }

    @Test
    public void getPostcodeShouldReturnProperObj() throws PostcodeNotFoundException {
        Postcode postcodeMock = Postcode.builder().latitude(57.14416516).longitude(-2.114847768).postCode("AB10 1XG").build();

        when(postcodeRepository.findBypostCode(anyString())).thenReturn(Optional.ofNullable(postcodeMock));

        PostcodeView postcodeView = locationFinderService.getPostcode("AB10 1XG");
        verify(postcodeRepository, times(1)).findBypostCode(anyString());
        assertThat(postcodeView, is(notNullValue()));
        assertThat(postcodeView.getPostcode(), is("AB10 1XG"));

    }

    @Test(expected = PostcodeNotFoundException.class)
    public void getPostcodeShouldThrownPostcodeNotFoundException() throws PostcodeNotFoundException {
        when(postcodeRepository.findBypostCode(anyString())).thenReturn(Optional.empty());
        locationFinderService.getPostcode("AB10 1XG");
        verify(postcodeRepository, times(1)).findBypostCode(anyString());
    }

}
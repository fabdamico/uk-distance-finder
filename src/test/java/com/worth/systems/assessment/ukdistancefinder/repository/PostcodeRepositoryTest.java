package com.worth.systems.assessment.ukdistancefinder.repository;

import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostcodeRepositoryTest {

    @Autowired
    private PostcodeRepository postcodeRepository;

    @Test
    public void findByPostCodeShouldReturnAnExistingObject() {
        String postcodeExample = "AB10 1XG";
        Optional<Postcode> postcode = postcodeRepository.findBypostCode(postcodeExample);
        assertThat(postcode.get(), is(notNullValue()));
        assertThat(postcode.get(), hasProperty("latitude", is(57.14416516)));
        assertThat(postcode.get(), hasProperty("longitude", is(-2.114847768)));
        assertThat(postcode.get(), hasProperty("postCode", is(postcodeExample)));
        assertThat(postcode.get(), hasProperty("id", is(1L)));
    }

    @Test
    public void findByPostCodeShouldReturnAnEmptyOptionalForNotFoundPostcode() {
        String fakePostcode = "AH10 123";
        Optional<Postcode> postcode = postcodeRepository.findBypostCode(fakePostcode);
        assertThat(postcode.isPresent(), is(false));
    }

    @Test
    public void findAllShouldReturnAnExistingObjectList() {
        List<Postcode> postcode = postcodeRepository.findAll();
        assertThat(postcode, is(notNullValue()));
        assertThat(postcode.size(), is(100));
    }

    @Test
    public void saveShouldPersistPostcodeObject() {
        Postcode postcodeToPersist = Postcode.builder()
                .postCode("123A 1G")
                .latitude(34.34566777)
                .longitude(-23.56745345)
                .build();

        postcodeRepository.save(postcodeToPersist);

        Optional<Postcode> existingPostcode = postcodeRepository.findBypostCode(postcodeToPersist.getPostCode());
        assertThat(existingPostcode.get(), is(notNullValue()));
        assertThat(existingPostcode.get(), hasProperty("latitude", is(postcodeToPersist.getLatitude())));
        assertThat(existingPostcode.get(), hasProperty("longitude", is(postcodeToPersist.getLongitude())));
        assertThat(existingPostcode.get(), hasProperty("postCode", is(postcodeToPersist.getPostCode())));

    }
}
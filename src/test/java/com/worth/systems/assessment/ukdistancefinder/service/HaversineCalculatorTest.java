package com.worth.systems.assessment.ukdistancefinder.service;

import com.worth.systems.assessment.ukdistancefinder.entity.Postcode;
import com.worth.systems.assessment.ukdistancefinder.util.HaversineCalculator;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class HaversineCalculatorTest {

    private HaversineCalculator haversineCalculator;
    private Postcode postcodeOne;
    private Postcode postcodeTwo;

    @Before
    public void setUp() {
        haversineCalculator = new HaversineCalculator();
        postcodeOne = Postcode.builder()
                .latitude(57.14416516)
                .longitude(-2.114847768)
                .postCode("AB10 1XG")
                .build();

        postcodeTwo = Postcode.builder()
                .latitude(57.13787976)
                .longitude(-2.121486688)
                .postCode("AB10 6RN")
                .build();
    }

    @Test
    public void calculateDistance() {
        double distance = haversineCalculator.calculateDistance(postcodeOne.getLatitude(), postcodeOne.getLongitude(),
                postcodeTwo.getLatitude(), postcodeTwo.getLongitude());

        assertThat(distance, is(0.8055408702920437));
    }

}